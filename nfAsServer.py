import BaseHTTPServer
import SocketServer
import os
import time
import mimetypes

import NfApi
from NfPlugin import NfPlugin


class SetupParams:
    def __init__(self):
        pass

    username = None
    password = None
    server = None
    basePath = None

    def from_argv(self, argv):
        # noinspection PyBroadException
        try:
            self.username, self.password = argv[1], argv[2]
        except:
            print "Usage - <username> <password> [server] [base path]"

        if len(argv) > 3:
            self.server = argv[3]

        if len(argv) > 4:
            self.basePath = argv[4]

        return self


params = SetupParams().from_argv(os.sys.argv)


class Context:
    def __init__(self):
        pass

    plugins = {
        "nf": NfPlugin()
    }

    nfApi = NfApi.Api()

    class Definitions:
        def __init__(self):
            self.locale = "en-us"

    definitions = Definitions()

    def locale(self):
        return self.definitions.locale

    def set_locale(self, locale):
        self.definitions.locale = locale
        return self

    def api(self):
        return self.nfApi


context = Context().set_locale("he-il")


# noinspection PyBroadException
class ServerHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    read_data = ""

    def server_killer(self):
        print "Killing the server"
        self.send_response(200)
        self.send_header("Content-type", "text/json")
        self.end_headers()
        self.server.server_close()

    def reloadData(self):
        print "Reloading"
        self.send_response(200)
        self.send_header("Content-type", "text/json")
        self.end_headers()
        context.nfApi.fetchConfigTroika()
        context.nfApi.fetchUserData()


    def do_OPTIONS(self):
        print "Serving: OPTIONS " + self.path
        self.send_response(200, "ok")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header('Access-Control-Allow-Headers',
                         'Content-Type, Authorization, X-Requested-With')
        # 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
        self.end_headers()

    def do_GET(self):
        print "Serving: GET " + self.path
        self.send_response(200)
        self.send_header("Content-type", self.get_mime(self.path))
        self.send_header("date", time.strftime("%d %h %Y - %H %M %S"))
        self.send_header("server", "AntiCordova")
        self.send_header('cache-control', 'max-age=10')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header('Access-Control-Allow-Headers',
                         'Content-Type, Authorization, X-Requested-With')
        self.end_headers()
        p = params.basePath + self.path
        if "?" in p:
            p = p[:p.find("?")]
        print "Fetching from path " + p
        try:
            contents = open(p, 'rb').read()
            self.wfile.write(contents)
        except:
            print "Failed to serve " + p

    def do_POST(self):
        print "Serving: POST " + self.path
        request_chain = [x for x in self.path.split('/') if len(x) > 0]
        if any([x in request_chain for x in ["getout", "briss", "takeoff"]]):
            self.server_killer()
            return

        if any([x in request_chain for x in ["reload", "sync"]]):
            self.reloadData()
            return

        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()

        self.read_data = self.rfile.read(int(self.headers['Content-Length']))
        # body = self.call_post(self.request)
        # self.wfile.write(json.dumps({"foo": 1, "bar": "2", "body": body}))
        api = context.api().set_locale(context.locale())

        try:
            plugin_name, command = request_chain[0].lower(), request_chain[1]
            print "Processing %s / %s" % (plugin_name, command)
            plugin = context.plugins[plugin_name]
            plugin.set_locale(context.locale())
            plugin.set_api(api)
            plugin.handle(self.wfile.write, command, self.read_data)

        except:
            self.wfile.write("")

    @staticmethod
    def get_mime(path):
        if not mimetypes.inited:
            mimetypes.init()

        try:
            if "?" in path:
                path = path[:path.find("?")]
            ext = path[path.rfind("."):]
            encoding = mimetypes.types_map[ext]
        except:
            encoding = "text/plain"

        return encoding


PORT = 8000
connected = False
httpd = None

api = context.api()
print "Auth"
api.auth(username=params.username, password=params.password, server=params.server)
# self.wfile.write(self.server_data.json())

print "Login"
api.login()

print "UserID = " + str(api.get_user_id()) + " OrgId = " + str(api.get_org_id()) + " AccessToken = " + str(api.get_server_data().token)
# self.wfile.write(self.user_data.json())

print "Fetching orgConfigs and the config troika"
api.fetchConfigTroika()

while not connected:
    # noinspection PyBroadException
    try:
        httpd = SocketServer.TCPServer(("", PORT), ServerHandler)
        connected = True
        print '-' * 30 + 'connected' + '-' * 30
    except:
        print "retrying"
        time.sleep(1)

# noinspection PyBroadException
try:
    httpd.serve_forever()
except:
    pass

httpd.server_close()
httpd.shutdown()
print "server is out"
