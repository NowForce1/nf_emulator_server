from CordovaPlugin import CordovaPlugin
import json
import re


# noinspection PyMethodMayBeStatic,PyBroadException,PyUnusedLocal
class NfPlugin(CordovaPlugin):
    locale = "en-us"

    def __init__(self):
        CordovaPlugin.__init__(self)

    def set_locale(self, locale):
        self.locale = locale
        return self

    def getCommunicationParams(self, command, data):
        server_data = self.api.get_server_data()
        response = {
            "Header": {
                "Authorization": "Bearer " + server_data.token,
                "Content-Type": "application/json"
            },
            "Region": {
                "NameKey": server_data.server_name,
                "SchemeKey": "https",
                "UrlKey": server_data.server_domain
            }
        }
        return json.dumps(response)

    def getInstalledNavApps(self, command, data):
        response = [{"appId": "WAZE", "appName": "Waze"}, {"appId": "GMAPS", "appName": "Maps"}]
        return json.dumps(response)

    def getAppLanga(self, command, data):
        response = {}
        for l in open("AppLanga.info").readlines():
            try:
                if re.match("\\s*//.*", l):
                    continue

                parsed = re.findall('(.*\\w)\\s{3,}\\s+(.*)', l)[0]
                response[parsed[0].strip()] = parsed[1].strip()
            except:
                pass

        return json.dumps(response)

    def setIncidentUserStatus(self, command, data):
        input_data = json.loads(data)["args"]
        result = self.api.reportStatus(input_data[0], input_data[1])
        return json.dumps(result)

    def getOrgConfig(self, command, data):
        orgConfigId = json.loads(data)["args"][0]
        return self.api.config_troika.getOrgConfig(orgConfigId)

    def getUserDetails(self, command, data):
        org = self.api.get_org_id()
        result = self.api.api_post("user/QueryUsers/json/%d/%d" % (org, org), {"UserId": self.api.get_user_id()})
        return json.dumps(result[0])

    def getLocale(self, command, data):
        return json.dumps({"Locale": self.locale})

    def getCurrentPosition(self, command, data):
        # NF office : 31.7504, 35.2070
        return json.dumps({"coords": {"latitude": 31.7504, "longitude": 35.2070}})
        # return json.dumps({"coords": {"latitude": 31.777, "longitude": 35.19}})

    # The iOS version
    def getCurrentLocation(self, command, data):
        return self.getCurrentPosition(command, data)

    def reportEvent(self, command, data):
        return "{}"

    def getVideoStatus(self, command, data):
        return 0

    def getAssetDataByAssetId(self, command, data):
        org = self.api.get_org_id()
        try:
            arguments = json.loads(data)["args"][0]
        except:
            arguments = -1

        result = self.api.api_get(
            "Assets/Query/json/%d/%d?AssetId=%d&CalcAssetActions=true" % (org, org, arguments))
        asset = result["Assets"][0]
        return json.dumps(asset)

    def associateNewIncidentWithAsset(self, command, data):
        org = self.api.get_org_id()
        incident_id = None
        asset_id = None
        try:
            arguments = json.loads(data)["args"]
            asset_id = arguments[0]
            incident_id = arguments[1]
        except:
            arguments = -1

        result = self.api.api_post(
            "Assets/upsert/json/%d/%d" % (org, org),
            {
                "UpdatePolicy": 1,
                "UpdateEntitiesPolicy": 1,
                "Asset": {
                    "AssetId": asset_id,
                    "AssetEntities": [
                        {
                            "AssetEntityTypeId": 13,
                            "EntityID": incident_id
                        }
                    ]
                }
            }
        )
        return 0

    def getIncidentDataFromApi(self, command, data):
        try:
            org = str(self.api.get_org_id())
            incidentId = str(json.loads(data)["args"][0])
            result = self.api.api_get(
                "incident/Query3/json/" + org + "/" + org + "?ShowIncidentDetails=true&ShowActiveUsersInIncident=true&ShowIncidentContactPersons=true&ShowLastIncidentComment=true&CalcDistance=true&IncidentId=" + incidentId + "&ShowAssets=true&AssetCategory=2")
            return json.dumps(result)
        except:
            return "{}"

    def getGeneralConfigItem(self, command, data):
        try:
            key = json.loads(data)["args"][0]
            return self.api.config_troika.getGeneralConfigs()[key]
        except:
            return "----"

    def scanBarcode(self, command, data):
        import time
        return json.dumps({"code": str(time.time())})
