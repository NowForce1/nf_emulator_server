import urllib2
import json
import base64


# noinspection PyBroadException,PyPep8Naming
class Api:
    locale = "en-us"

    def __init__(self):
        self.user_data = UserData()
        self.config_troika = ConfigTroika()
        self.server_data = ServerData()
        pass

    def set_locale(self, locale):
        self.locale = locale
        return self

    def reportStatus(self, incident_id, status_code):
        org_id = self.get_org_id()
        report_state_url = "incident/SetIncidentStatus/json/%d/%d" % (org_id, org_id)

        payload = {
            "IncidentId": incident_id,
            "UserId": self.get_user_id(),
            "StatusId": status_code,
            "Age": 10
        }
        return self.api_post(report_state_url, payload)

    def get_user_id(self):
        return self.user_data.get("UserId")

    def get_org_id(self):
        org_id = self.user_data.get("OrganizationId")
        return org_id

    def api_get(self, uri):
        headers = {"Authorization": "Bearer " + self.server_data.token, "Content-Type": "application/json"}
        result = self.call_get("https://%s/api/%s/%s" % (self.server_data.server_domain, self.locale, uri), headers)
        if result:
            try:
                return json.loads(result.read())
            except:
                return None

    def api_post(self, uri_ext, data):
        headers = {"Authorization": "Bearer " + self.server_data.token, "Content-Type": "application/json"}
        result = self.call_post("https://%s/api/%s/%s"
                                % (self.server_data.server_domain, self.locale, uri_ext), json.dumps(data), headers)
        if result:
            try:
                return json.loads(result.read())
            except:
                return None

    @staticmethod
    def call_get(uri, headers=None):
        if headers is None:
            headers = {}
        req = urllib2.Request(url=uri, headers=headers)
        try:
            return urllib2.urlopen(req)
        except:
            return None

    @staticmethod
    def call_post(uri, data, headers=None):
        if headers is None:
            headers = {}
        req = urllib2.Request(uri, data, headers)
        try:
            return urllib2.urlopen(req)
        except:
            return None

    def auth(self, username, password, server):
        body = "grant_type=password&username=" + username + "&password=" + password + "&app_type=mobileApp"
        header_token = base64.b64encode(username + ";" + password)
        headers = {"Authorization": "Basic " + header_token, "Content-Type": "application/x-www-form-urlencoded"}
        if server:
            uri = "https://" + server + "/api/en-us/OAuth/AccessToken"
        else:
            uri = "https://oauth.nowforce.com/api/en-us/OAuth/AccessToken"

        result = self.call_post(uri, body, headers)
        if result:
            try:
                auth_result = json.loads(result.read())
                self.server_data.set(server_name=auth_result["ServerName"], server_domain=auth_result["LoginUrl"],
                                     token=auth_result["access_token"])
            except:
                return None

    def login(self):
        try:
            login_result = self.api_post("User/Login/json/-1/-1",
                                         {"DeviceId": "PYTHON_BOGUS_DEVICE", "AppType": 1, "InstalledVersion": "0.0.0"})
            self.user_data.set_user_details(login_result)
        except:
            return None

    def fetchConfigTroika(self):
        try:
            org_id = self.get_org_id()
            url = "User/Configurations2/json/%d/%d?OrganizationConfigurations=true&CodeTableDataItems=true&General=true&CodeTableId=5" % (
                org_id, org_id)
            troika_result = self.api_get(url)
            self.config_troika.setOrgConfigs(troika_result["OrganizationConfigurations"])
            self.config_troika.setGeneralConfigs(troika_result["General"])
            return troika_result
        except:
            return None

    def fetchUserData(self):
        try:
            org_id = str(self.get_org_id())
            url = "user/QueryUsers/json/" + org_id + "/" + org_id
            result = self.api_post(url, {"UserId": self.get_user_id()})[0]
            self.user_data.set_user_details(result)
            return result
        except:
            return None

    def get_server_data(self):
        return self.server_data

    def get_user_data(self):
        return self.user_data


class ServerData:
    def __init__(self):
        pass

    server_name = None,
    server_domain = None,
    token = None

    def authenticated(self):
        return self.token and self.server_domain

    def set(self, server_name, server_domain, token):
        self.server_name, self.server_domain, self.token = server_name, server_domain, token

    def json(self):
        json.dumps({"token": self.token, "domain": self.server_domain})


class UserData:
    def __init__(self):
        pass

    user_details = None

    def has_login(self):
        return bool(self.user_details)

    def set_user_details(self, user_details):
        # noinspection PyBroadException
        try:
            self.user_details = user_details
        except:
            print "---- FAILED TO SET USER DETAILS ----"

    def get(self, key):
        # noinspection PyBroadException
        try:
            return self.user_details["MobileUser"][key]
        except:
            return None

    def json(self):
        return json.dumps({"user_details": self.user_details})


class ConfigTroika:
    def __init__(self):
        pass

    org_configs = {}
    general_configs = {}

    def setOrgConfigs(self, org_configs):
        self.org_configs = dict(map(lambda x: (x["ConfigurationId"], x["ConfigurationValue"]), org_configs))

    def getOrgConfig(self, org_config_id):
        if org_config_id in self.org_configs:
            return self.org_configs[org_config_id]
        else:
            return u""

    def setGeneralConfigs(self, general_config):
        self.general_configs = dict(map(lambda x: (x["Key"], x["Value"]), general_config))

    def getGeneralConfigs(self):
        return self.general_configs
