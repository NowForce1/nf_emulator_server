

# noinspection PyBroadException
class CordovaPlugin:
    def __init__(self):
        self.user_data = {}
        self.server_data = {}
        self.api = None

    def set_api(self, api):
        self.api = api
        self.server_data = self.api.get_server_data()
        self.user_data = self.api.get_user_data()

    def handle(self, write_method, command, post_data):
        try:
            method = getattr(self, command)
            write_method(method(command, post_data))
        except:
            print ">"*10 + "%s/%s cannot be served"%(command, self.__class__.__name__) + "<"*10
            pass

        return self



